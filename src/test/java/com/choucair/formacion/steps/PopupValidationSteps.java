package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {
	
	ColorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage colorlibMenuPage;
	
	
	@Step 
	
	public void login_colorlib (String strUsuario, String strPass) {
		
		// a.abrir navegador con la url de prueba
		colorlibLoginPage.open();
		// b. ingresar usuario demo
		// c. ingresar password demo
		// d. click boton sing in *\
		
		colorlibLoginPage.IngresarDatos (strUsuario, strPass);
		// e. verificar la autenticacion  (label en home)
		colorlibLoginPage.VerificaHome();
		
	}
	
	@Step
	public void ingresar_form_validation() {
		colorlibMenuPage.menuFormValidation();
	}
 
}
