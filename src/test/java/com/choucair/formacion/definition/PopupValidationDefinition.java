package com.choucair.formacion.definition;


import java.util.List;

import com.choucair.formacion.steps.PopupValidationSteps;
import com.choucair.formacion.steps.colorlibFormValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;


public class PopupValidationDefinition {
	
	@Steps
	PopupValidationSteps popupValidationSteps;
	@Steps
	colorlibFormValidationSteps colorlibFormValidationSteps;
	
	@Given("^autenticar en colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autenticar_en_colorlib_con_usuario_y_clave(String Usuario, String Clave)  {
	    // Write code here that turns the phrase above into concrete actions
		popupValidationSteps.login_colorlib(Usuario, Clave);
	   
	}

	@Given("^ingreso a la funcionalidad Forms Validation$")
	public void ingreso_a_la_funcionalidad_Forms_Validation() {
		popupValidationSteps.ingresar_form_validation();
	    
	    
	}

	@When("^Diligencio formulario popup Validation$")
	public void diligencio_formulario_popup_Validation(DataTable dtDatosForm) throws Throwable {
		List<List<String>> data = dtDatosForm.raw();
		for (int i=1; i<data.size(); i++) {
			colorlibFormValidationSteps.diligenciar_popup_datos_tabla(data, i);
			try {
				Thread.sleep(5000);
				} catch (InterruptedException e) {}
			
		}
	}
	       
	

	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso()  {
		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_exitoso();
	    // Write code here that turns the phrase above into concrete actions
	   
	}
	
	@Then("^Verificar que se presenta Globo Informativo de validacion$")
	public void verificar_que_se_presenta_Globo_Informativo_de_validacion() {
		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_con_errores();
}

}
