package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class ColorlibFormValidationPage extends PageObject{
	
	//campo required
	@FindBy(xpath="//*[@id='req']")
	public WebElementFacade txtRequired;
	
	//campo select sport 1
	@FindBy(xpath="//*[@id='sport']")
	public WebElementFacade cmbSport1;
	
	//campo url
	@FindBy(xpath="//INPUT[@id='url1']")
	public WebElementFacade txtUrl;
	
	//campo e-mail
	@FindBy(id="email1")
	public WebElementFacade txtEmail1;
	
	//campo Password
	@FindBy(id="pass1")
	public WebElementFacade txtPass1;
	
	//campo Confirm password
	@FindBy(id="pass2")
	public WebElementFacade txtPass2;
	
	//campo Minimun fiel size (6)
	@FindBy(id="minsize1")
	public WebElementFacade txtMinisize1;
	
	//campo maximun field size, optional
	@FindBy(id="maxsize1")
	public WebElementFacade txtMaxsize;
	
	//campo number
	@FindBy(id="number2")
	public WebElementFacade txtNumber;
	
	//campo ip
	@FindBy(id="ip")
	public WebElementFacade txtIp;
	
	//campo date
	@FindBy(id="date3")
	public WebElementFacade txtDate;
	
	//campo date ealier
	@FindBy(id="past")
	public WebElementFacade txtDataEarlier;
	
	//campo seleccion sport 2
	@FindBy(id="sport2")
	public WebElementFacade cmbSport2;
	
	public void Multiple_Select (String datoPrueba) {
		cmbSport2.selectByVisibleText(datoPrueba);
	}
	
	//boton validate 
	@FindBy(xpath="//*[@id=\"popup-validation\"]/div[14]/input")
	public WebElementFacade btnValidate;
	//*[@id="popup-validation"]/div[14]/input
	//campo globo informativo 
	@FindBy(xpath="(//DIV*@class='formErrorContent'])[1]")
	public WebElementFacade globoInformativo;
	
	public void formulario_sin_errores() {
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
	}
	
	public void formulario_con_errores() {
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
	}
	
	
	public void Required (String datoPrueba) {
		txtRequired.click();
		txtRequired.clear ();
		txtRequired.sendKeys(datoPrueba);
			}
	
	public void Select_Sport (String datoPrueba) {
		cmbSport1.click();
		cmbSport1.selectByVisibleText (datoPrueba);
	}
	
	public void Multiple_Select1 (String datoPrueba) {
		cmbSport2.click();
		cmbSport2.selectByVisibleText (datoPrueba);
	}
	public void url (String datoPrueba) {
		txtUrl.click();
		txtUrl.clear ();
		txtUrl.sendKeys(datoPrueba);
	}
	
	public void Email (String datoPrueba) {
		txtEmail1.click();
		txtEmail1.clear ();
		txtEmail1.sendKeys(datoPrueba);
	}
	
	public void password (String datoPrueba) {
		txtPass1.click();
		txtPass1.clear ();
		txtPass1.sendKeys(datoPrueba);
	}
	
	public void confirm_password (String datoPrueba) {
		txtPass2.click();
		txtPass2.clear ();
		txtPass2.sendKeys(datoPrueba);
	}
	
	public void miniSize (String datoPrueba) {
		txtMinisize1.click();
		txtMinisize1.clear ();
		txtMinisize1.sendKeys(datoPrueba);
	}
	
	public void maxiSize (String datoPrueba) {
		txtMaxsize.click();
		txtMaxsize.clear ();
		txtMaxsize.sendKeys(datoPrueba);
	}
	
	public void Number (String datoPrueba) {
		txtNumber.click();
		txtNumber.clear ();
		txtNumber.sendKeys(datoPrueba);
	}
	
	public void date (String datoPrueba) {
		txtDate.click();
		txtDate.clear ();
		txtDate.sendKeys(datoPrueba);
	}
	
	public void date_earlier(String datoPrueba) {
		txtDataEarlier.click();
		txtDataEarlier.clear ();
		txtDataEarlier.sendKeys(datoPrueba);
	}
	
	
	public void Ip (String datoPrueba) {
		txtIp.click();
		txtIp.clear ();
		txtIp.sendKeys(datoPrueba);
	}
	
	public void validate () {
		btnValidate.click();
	}
 
}

