package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString; 

public class ColorlibMenuPage extends PageObject {
	
	//menu formas
	@FindBy(xpath="(//A[@href='javascript:;'])[3]")
	public WebElementFacade menuForms;
	
	//Submenu forms validacion
	@FindBy(xpath="(//A[@href='form-validation.html'])[2]")
	public WebElementFacade menuFormValidation;
	
	//popup validation
	@FindBy(xpath="(//H5[text()='Popup Validation'])[1]")
	public WebElementFacade lblFormValidation;
	
	public void menuFormValidation() {
		menuForms.click();
		menuFormValidation.click();
		String strMensaje = lblFormValidation.getText();
		assertThat(strMensaje, containsString("Popup Validation"));
		
	}

}
