package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.*;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")

public class ColorlibLoginPage extends PageObject {
	
	//campo de usuario
	
	@FindBy(xpath="//*[@id='login']/form/input[1]")
	public WebElementFacade txtUsername;
    
	//campo de contraseña
	@FindBy(xpath="//*[@id=\"login\"]/form/input[2]")
	public WebElementFacade txtPassword;
	
	//boton Sing in
	@FindBy(xpath="//*[@id=\"login\"]/form/button")
	public WebElementFacade btnSingin;
	
	//entrar a la pagina
	@FindBy(xpath="//*[@id=\'bootstrap-admin-template\']")
	public WebElementFacade lblHomePpal;
	
	//globo informativo
	@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
	public WebElementFacade globoInformativo;
	
	public void form_sin_errores() {
		assertThat(globoInformativo.isCurrentlyVisible(),is (false));
	}
	
	public void from_con_errores () {
		assertThat(globoInformativo.isCurrentlyVisible(),is (true));
		
	}
	public void IngresarDatos(String strUsuario, String strPass) {
		txtUsername.sendKeys(strUsuario);
		txtPassword.sendKeys(strPass);
		btnSingin.click();
	}
	
	public void VerificaHome() {
		String labelv= "Bootstrap-Admin-Template";
		String strMensaje= lblHomePpal.getText();
		assertThat(strMensaje, containsString(labelv));
		System.out.println("exito");
		
	}
}



